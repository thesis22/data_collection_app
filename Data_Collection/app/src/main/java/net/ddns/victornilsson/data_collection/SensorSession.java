package net.ddns.victornilsson.data_collection;

import android.content.ContentValues;

import java.util.Date;

import static android.os.SystemClock.elapsedRealtimeNanos;

public class SensorSession {

    private int mSessionId;
    private SensorSessionType.SessionTypes mType;

    public SensorSession(SensorSessionType.SessionTypes type){
        mType = type;
    }

    public void setSessionId(int sessionId) {
        mSessionId = sessionId;
    }

    public int getSessionId(){
        return mSessionId;
    }

    public ContentValues getContentValuesSession(){
        ContentValues values = new ContentValues();
        values.put(SensorDBSchema.SensorSessionTable.Cols.ID, this.mSessionId);
        values.put(SensorDBSchema.SensorSessionTable.Cols.SESSION_TYPE, this.mType.mIndex);
        return values;
    }
}
