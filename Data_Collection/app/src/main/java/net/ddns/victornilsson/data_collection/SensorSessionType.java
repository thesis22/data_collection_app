package net.ddns.victornilsson.data_collection;

import android.content.ContentValues;
import android.content.Context;

public class SensorSessionType {

    public enum SessionTypes{
        running("Running", 0),
        cycling("Cycling", 1),
        speedWalking("Speed Walking", 2),
        walking("walking", 3);

        public final String mLabel;
        public final int mIndex;

        private SessionTypes(String label, int index){
            this.mLabel = label;
            this.mIndex = index;
        }
    }

    public static void addSessionTypesToDB(Context context){
        for (SessionTypes sessionType : SessionTypes.values()){
            ContentValues values = new ContentValues();
            values.put(SensorDBSchema.SensorSessionTypeTable.Cols.ID, sessionType.mIndex);
            values.put(SensorDBSchema.SensorSessionTypeTable.Cols.NAME, sessionType.mLabel);
            SensorReaderSaver.get(context).queueSensorSessionTypeSave(values);
        }
    }
}
