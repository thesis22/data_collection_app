package net.ddns.victornilsson.data_collection.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.util.Log;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import net.ddns.victornilsson.data_collection.fragments.SensorListFragment;

public class SensorListActivity extends SingleFragmentActivity {

    private static final String TAG = "SensorListActivity";
    private static final int REQUEST_WAKE_LOCK = 0;
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected Fragment createFragment() {
        return new SensorListFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // https://stackoverflow.com/questions/17400940/is-it-possible-to-detect-motion-when-screen-is-off
        Log.i(TAG, "onStart()");
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WAKE_LOCK}, REQUEST_WAKE_LOCK);
        } else {
            acquireWakeLock();
        }


    }

    private void acquireWakeLock(){
        Log.i(TAG, "acquireWakeLock()");
        PowerManager pm = (PowerManager) getSystemService(getApplicationContext().POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag:");

        mWakeLock.acquire();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WAKE_LOCK:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    acquireWakeLock();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i(TAG, "onDestroy()");
        mWakeLock.release();
    }
}
