package net.ddns.victornilsson.data_collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SensorLab {
    private static final String TAG = "SensorLab";

    private static SensorLab sSensorLab;
    private final SensorManager mSensorManager;
    private Context mContext;
    private List<SensorReader> mSensorReaders;
    private SensorSessionType.SessionTypes mSessionType;



    public static SensorLab get(Context context){
        if (sSensorLab == null){
            sSensorLab = new SensorLab(context);
        }
        return sSensorLab;
    }

    private SensorLab(Context context){
        mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        mSensorReaders = new ArrayList<>();
        mContext = context.getApplicationContext();
        SensorSessionType.addSessionTypesToDB(mContext);

        for (Sensor sensor : mSensorManager.getSensorList(Sensor.TYPE_ALL)){
            // We do not consider uncalibrated sensors
            if (sensor.getName().toLowerCase().contains("uncalibrated")) {
                continue;
            }
            SensorReader sensorReader = new SensorReader(mSensorManager, sensor, mContext);
            mSensorReaders.add(sensorReader);
        }
    }


    public List<SensorReader> getSensorReaders(){
        return mSensorReaders;
    }

    public SensorReader getSensor(int id){
        for (SensorReader sensorReader : mSensorReaders){
            if (sensorReader.getType()== id){
                return sensorReader;
            }
        }
        return null;
    }

    public SensorReader getSensor(String name){
        for (SensorReader sensorReader : mSensorReaders){
            if (sensorReader.getName().equals(name)){
                return sensorReader;
            }
        }
        return null;
    }

    public SensorSessionType.SessionTypes getSessionType() {
        return mSessionType;
    }

    public void setSessionType(SensorSessionType.SessionTypes sessionType) {
        mSessionType = sessionType;
    }
}
