package net.ddns.victornilsson.data_collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

public class SensorReaderSaver extends HandlerThread {
    private static final String TAG = "SensorReaderSaver";

    private static SensorReaderSaver sSensorReaderSaver;
    private static int sNumberInQueue = 0;
    private Context mContext;
    private SQLiteDatabase mSQLiteDatabase;

    private static final int MESSAGE_SENSOR = 0;
    private static final int MESSAGE_SENSOR_READING = 1;
    private static final int MESSAGE_SESSION_TYPE = 2;
    private static final int MESSAGE_SESSION = 3;
    private Handler mSaveHandler;

    private Timer mSaveDataTimer;
    private int mTimerUpdate = 1000;
    private final int mAllowSaveCycles = 2;
    private final int mStopSaveCycles = 15;
    private boolean mAllowSave = true;
    private int mCycle = 0;
    private SensorSession mSensorSession = null;
    private int mSessionSequence = 0;

    public static SensorReaderSaver get(Context context){
        if (sSensorReaderSaver == null){
            sSensorReaderSaver = new SensorReaderSaver(context);
        }
        return sSensorReaderSaver;
    }

    private SensorReaderSaver(Context context) {
        super(TAG);
        this.mContext = context;
        this.mSQLiteDatabase = new SensorDataBaseHelper(mContext)
                .getWritableDatabase();
        mSaveDataTimer = new Timer();
        mSaveDataTimer.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run() {
                int compareCycle;
                if (mAllowSave){
                    compareCycle = mAllowSaveCycles;
                }
                else {
                    compareCycle = mStopSaveCycles;
                }
                if (mCycle == compareCycle){
                    mAllowSave = !mAllowSave;
                    mCycle=0;
                    if (mAllowSave){
                        mSessionSequence++;
                    }
                }
                mCycle++;
            }
        }, mTimerUpdate,mTimerUpdate);
    }


    @Override
    public boolean quit() {
        return super.quit();
    }

    @Override
    protected void onLooperPrepared() {
        mSaveHandler = new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                ContentValues values = (ContentValues)msg.obj;
                sNumberInQueue--;
                Log.i(TAG, "handleMessage: sNumberInQueue: " + sNumberInQueue);
                Log.i(TAG, "handleMessage: messageType: " + msg.what);
                switch (msg.what){
                    case MESSAGE_SENSOR:
                        saveSensor(values);
                        break;
                    case MESSAGE_SENSOR_READING:
                        saveSensorReading(values);
                        break;
                    case MESSAGE_SESSION_TYPE:
                        saveSessionType(values);
                        break;
                    case  MESSAGE_SESSION:
                        saveSession(values);
                        break;
                    default:
                        Log.i(TAG, "handleMessage: unhandled message type: " + msg.what);
                }
            }

        };
    }

    public void clearQueue(){
        mSaveHandler.removeMessages(MESSAGE_SENSOR);
        mSaveHandler.removeMessages(MESSAGE_SENSOR_READING);
    }



    private void saveSessionType(ContentValues contentValues){
        mSQLiteDatabase.insertWithOnConflict(SensorDBSchema.SensorSessionTypeTable.NAME,
                null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
    }

    private void saveSession(ContentValues contentValues){
        mSQLiteDatabase.insertWithOnConflict(SensorDBSchema.SensorSessionTable.NAME,
                null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
    }

    private void saveSensor(ContentValues contentValues){
        mSQLiteDatabase.insertWithOnConflict(SensorDBSchema.SensorTable.NAME,
                null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
    }
    private void saveSensorReading(ContentValues contentValues){
        mSQLiteDatabase.insert(SensorDBSchema.SensorValueTable.NAME, null, contentValues);
    }

    public void queueSensorReaderSave(ContentValues contentValues){
        if (mAllowSave && mSensorSession != null) {
            sNumberInQueue++;
            contentValues.put(SensorDBSchema.SensorValueTable.Cols.SESSION_ID, mSensorSession.getSessionId());
            contentValues.put(SensorDBSchema.SensorValueTable.Cols.SESSION_SEQUENCE_ID, mSessionSequence);
            mSaveHandler.obtainMessage(MESSAGE_SENSOR_READING, contentValues)
                    .sendToTarget();
        }
    }

    public void queueSensorSave(ContentValues contentValues){
        sNumberInQueue++;
        mSaveHandler.obtainMessage(MESSAGE_SENSOR, contentValues)
                .sendToTarget();
    }

    public void queueSensorSessionTypeSave(ContentValues contentValues){
        sNumberInQueue++;
        mSaveHandler.obtainMessage(MESSAGE_SESSION_TYPE, contentValues)
                .sendToTarget();
    }
    public void queueSensorSessionSave(ContentValues contentValues){
        sNumberInQueue++;
        mSaveHandler.obtainMessage(MESSAGE_SESSION, contentValues)
                .sendToTarget();
    }

    public void setSensorSessionType(SensorSessionType.SessionTypes sensorSession) {
        mSensorSession = new SensorSession(sensorSession);
        mSensorSession.setSessionId(getLatestSessionID()+1);
        queueSensorSessionSave(mSensorSession.getContentValuesSession());
    }

    public int getLatestSessionID(){
        SQLiteDatabase _SQLiteDatabase = new SensorDataBaseHelper(mContext)
                .getReadableDatabase();
        StringBuilder builder = new StringBuilder("SELECT ");
        builder.append( SensorDBSchema.SensorSessionTable.Cols.ID)
                .append(" FROM ").append(SensorDBSchema.SensorSessionTable.NAME)
                .append(" ORDER BY ").append(SensorDBSchema.SensorSessionTable.Cols.ID).append(" DESC ")
                .append("LIMIT 1");
        String query = builder.toString();

        Cursor cursor = _SQLiteDatabase.rawQuery(query, new String[]{});
        int id = -1;
        try {
            int index = cursor.getColumnIndex(SensorDBSchema.SensorSessionTable.Cols.ID);
            cursor.moveToFirst();
            id = cursor.getInt(index);
        } catch (Exception e){
            Log.i(TAG, "Unable to parse integer in get latest session id: " + e.getMessage());
        } finally{
            cursor.close();
            Log.i(TAG, "REturning id: " + id + " from getLatestSessionID()" );
            return id;
        }
    }
}
