package net.ddns.victornilsson.data_collection.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.ddns.victornilsson.data_collection.R;
import net.ddns.victornilsson.data_collection.SensorReader;
import net.ddns.victornilsson.data_collection.SensorLab;
import net.ddns.victornilsson.data_collection.SensorReaderSaver;
import net.ddns.victornilsson.data_collection.SensorSession;
import net.ddns.victornilsson.data_collection.SensorSessionType;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SensorListFragment extends Fragment {
    private static final String TAG = "SensorListFragment";
    private RecyclerView mSensorRecyclerView;
    private SensorAdapter mAdapter;
    private Timer mUpdateListItemTimer;
    private int mUpdateTimerPeriod = 1000;
    private SensorReaderSaver mSensorReaderSaver;
    private List<SensorReader> mSensorReaders;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensor_list, container, false);
        setHasOptionsMenu(true);

        mSensorRecyclerView = view.findViewById(R.id.sensor_recycler_view);
        mSensorRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSensorReaderSaver = SensorReaderSaver.get(getContext());
        mSensorReaderSaver.start();
        mSensorReaderSaver.getLooper();
        Log.i(TAG, "Background sensor saver started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSensorReaderSaver.quit();
        for (SensorReader reader : mSensorReaders){
            reader.setRunning(false);
        }
        Log.i(TAG, " Background sensor saver destroyed");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_sensor_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.start_all_sensors:
                activateAllSensors();
                return true;
            case R.id.stop_all_sensors:
                deactivateAllSensors();
                return true;
            case R.id.start_running_session:
                startNewSession(SensorSessionType.SessionTypes.running);
                return true;
            case R.id.start_cycling_session:
                startNewSession(SensorSessionType.SessionTypes.cycling);
                return true;
            case R.id.start_fast_walking_session:
                startNewSession(SensorSessionType.SessionTypes.speedWalking);
                return true;
            case R.id.start_walking_session:
                startNewSession(SensorSessionType.SessionTypes.walking);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startNewSession(SensorSessionType.SessionTypes sessionType){
        SensorReaderSaver.get(getContext()).setSensorSessionType(sessionType);
    }

    private void activateAllSensors(){
        for (SensorReader reader : mSensorReaders){
            reader.setRunning(true);
        }
    }

    private void deactivateAllSensors(){
        for (SensorReader reader : mSensorReaders){
            reader.setRunning(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSensorReaderSaver.clearQueue();
    }

    private void updateUI(){
        SensorLab sensorLab = SensorLab.get(getActivity());
        mSensorReaders = sensorLab.getSensorReaders();

        mAdapter = new SensorAdapter(mSensorReaders);
        mSensorRecyclerView.setAdapter(mAdapter);
        mUpdateListItemTimer = new Timer();
        FragmentActivity activity = this.getActivity();
        mUpdateListItemTimer.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < mSensorReaders.size(); i++){
                            mAdapter.notifyItemChanged(i);
                        }
                    }
                });
            }
        }, mUpdateTimerPeriod,mUpdateTimerPeriod);
    }


    private class SensorHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private TextView mTitleTextView;
        private TextView mDetailsTextView;
        private ImageView mSensorRunningImageView;

        private SensorReader mSensorReader;

        public SensorHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.list_item_sensor, parent, false));

            mTitleTextView = itemView.findViewById(R.id.sensor_title);
            mDetailsTextView = itemView.findViewById(R.id.sensor_details);
            mSensorRunningImageView = itemView.findViewById(R.id.sensor_running_imageview);
            itemView.setOnClickListener(this);
        }

        public void bind(SensorReader sensorReader){
            mSensorReader = sensorReader;
            updateText();
        }

        private void updateText(){
            mTitleTextView.setText(mSensorReader.getName());
            mDetailsTextView.setText(mSensorReader.toString());
            mSensorRunningImageView.setImageResource(!mSensorReader.isRunning() ?
                    R.drawable.ic_play_button :
                    R.drawable.ic_stop_button);
            mSensorRunningImageView.setColorFilter(ContextCompat.getColor(
                    getActivity(),
                    mSensorReader.isRunning() ?
                    R.color.color_primary :
                    R.color.color_secondary));
        }

        @Override
        public void onClick(View v) {
            mSensorReader.toggleRunning();
            updateText();
        }
    }


    private class SensorAdapter extends RecyclerView.Adapter<SensorHolder>{
        private List<SensorReader> mSensorReaders;

        public SensorAdapter(List<SensorReader> sensorReaders){
            mSensorReaders = sensorReaders;
        }

        @NonNull
        @Override
        public SensorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new SensorHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull SensorHolder holder, int position) {
            SensorReader sensorReader = mSensorReaders.get(position);
            holder.bind(sensorReader);
        }



        @Override
        public int getItemCount() {
            return mSensorReaders.size();
        }
    }
}
