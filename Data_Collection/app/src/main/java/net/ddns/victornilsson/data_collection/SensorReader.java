package net.ddns.victornilsson.data_collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.Log;

import java.util.Date;

import static android.os.SystemClock.elapsedRealtimeNanos;

public class SensorReader {
    private static final String TAG = "SensorReader";
    private static int sNumberOfSensors = 0;
    private String mName;
    private int mType;
    private boolean mRunning;
    private final SensorEventListener mSensorEventListener;
    private SensorEvent mLatestSensorEvent = null;
    private final SensorManager mSensorManager;
    private final Sensor mSensor;
    private final Context mContext;

    public SensorReader(final SensorManager sensorManager, final Sensor sensor, final Context context){
        mContext = context;
        mSensor = sensor;
        mSensorManager = sensorManager;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mType = mSensor.getType();
        } else {
            mType = sNumberOfSensors;
        }
        sNumberOfSensors ++;
        mName = mSensor.getName();
        mSensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                mLatestSensorEvent = event;
                updateDatabaseValues();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {}
        };

        updateDatabaseMetaData();
        setRunning(false);
    }

    private void updateDatabaseValues(){
        int sensorLength = mLatestSensorEvent.values.length;
        for (int i = 0; i < sensorLength; i++){
            long timestamp = getSensorTimestamp();
            ContentValues values = getContentValuesSenorValues(i, timestamp);
            SensorReaderSaver.get(mContext).queueSensorReaderSave(values);
        }
    }

    private long getSensorTimestamp(){
        // the latest sensor event timestamp is calculated in nanoseconds of uptime and not actual real time
        // so we convert it to a unix timeformat
        long SensorEventDiff =(mLatestSensorEvent.timestamp - elapsedRealtimeNanos() ) / 1000000L;
        long timestamp = (new Date()).getTime()
                + SensorEventDiff;
        return timestamp;
    }

    private void updateDatabaseMetaData(){
        ContentValues values = getContentValuesSensor();
        SensorReaderSaver.get(mContext).queueSensorSave(values);
    }

    private ContentValues getContentValuesSensor(){
        ContentValues values = new ContentValues();
        values.put(SensorDBSchema.SensorTable.Cols.NAME, this.getName());
        values.put(SensorDBSchema.SensorTable.Cols.TYPE, this.getType());
        return values;
    }

    private ContentValues getContentValuesSenorValues(int valueIndex, long timestamp){
        ContentValues values = new ContentValues();
        values.put(SensorDBSchema.SensorValueTable.Cols.TYPE, this.getType());

        values.put(SensorDBSchema.SensorValueTable.Cols.TIMESTAMP, timestamp);
        values.put(SensorDBSchema.SensorValueTable.Cols.VALUE, this.mLatestSensorEvent.values[valueIndex]);
        values.put(SensorDBSchema.SensorValueTable.Cols.VALUE_INDEX, valueIndex);
        return values;
    }


    /**
     * The toString method generates a string containing the sensors latest readings
     * sensorreadings
     * @return A string containing sensorreadings of the sensor.
     */
    public String toString(){
        StringBuilder builder = new StringBuilder();
        if (mLatestSensorEvent == null){
            return "";
        }
        builder.append("Values:");
        for (float value : mLatestSensorEvent.values ){
            builder.append(" ").append(value).append(",");
        }
        // remove trailing comma ','
        builder.setLength(builder.length()-1);
        builder.append("   ").append("Accuracy: ").append(mLatestSensorEvent.accuracy);
        builder.append("   ").append("Timestamp: ").append(mLatestSensorEvent.timestamp);
        builder.append("   ").append("Sensor Type: ").append(mType);
        return builder.toString();
    }

    public String getName() {
        return mName;
    }

    public int getType() {
        return mType;
    }

    public boolean isRunning() {
        return mRunning;
    }

    public void setRunning(boolean running) {
        mRunning = running;
        if (mRunning){
            startRunning();
        } else {
            stopRunning();
        }
    }

    private void stopRunning(){
        mSensorManager.unregisterListener(mSensorEventListener);
    }

    private void startRunning(){
        mSensorManager.registerListener(mSensorEventListener,
                mSensor,
                SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void toggleRunning(){
        setRunning(!isRunning());
    }
}
