package net.ddns.victornilsson.data_collection;

public class SensorDBSchema {
    public static final class SensorValueTable{
        public static final String NAME = "sensorValues";

        public static final class Cols {
            // Foreign key to SensorTable.Cols.TYPE
            public static final String TYPE = "type";
            public static final String TIMESTAMP = "timestamp";
            public static final String VALUE = "value";
            public static final String VALUE_INDEX = "value_index";
            // Foreign key to SensorSessionTable.Cols.id
            public static final String SESSION_ID = "session";
            public static final String SESSION_SEQUENCE_ID = "session_sequence";
        }
    }

    public static final class SensorSessionTypeTable{
        public static final String NAME = "sensorSessionType";

        public static final class Cols {
            // Primary key
            public static final String ID = "id";
            public static final String NAME = "name";

        }
    }

    public static final class SensorSessionTable{
        public static final String NAME = "sensorSession";

        public static final class Cols {
            // Primary key
            public static final String ID = "id";
            // Foreign key to SensorSessionTypeTable.Cols.ID
            public static final String SESSION_TYPE = "type";

        }
    }


    // Should only be accessed once.
    public static final class SensorTable{
        public static final String NAME = "sensors";
        public static final class Cols {
            // primary key
            public static final String TYPE = "type";
            public static final String NAME = "name";
        }
    }

}
