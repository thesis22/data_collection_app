package net.ddns.victornilsson.data_collection;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SensorDataBaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "SensorDataBaseHelper";
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "SensorDataBase.db";

    public SensorDataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create the sensortable
        db.execSQL(getCreateSensorTable());

        // Create the SensorSessionTypeTable
        db.execSQL(getCreateSensorSessionTypeTable());

        // Create the SensorSessionTable
        db.execSQL(getCreateSensorSessionTable());

        // create the sensorvaluetable
        db.execSQL(getCreateSensorValueTable());
    }




    private String getCreateSensorSessionTable(){
        StringBuilder builder = new StringBuilder("Create table ");
        builder.append(SensorDBSchema.SensorSessionTable.NAME).append("( ")
                .append(SensorDBSchema.SensorSessionTable.Cols.ID).append(" integer primary key autoincrement, ")
                .append(SensorDBSchema.SensorSessionTable.Cols.SESSION_TYPE).append(", ")
                .append("FOREIGN KEY(").append(SensorDBSchema.SensorSessionTable.Cols.SESSION_TYPE)
                .append(") REFERENCES ").append(SensorDBSchema.SensorSessionTypeTable.NAME).append("(")
                .append(SensorDBSchema.SensorSessionTypeTable.Cols.ID).append(")")
                .append(")");
        return builder.toString();
    }

    private String getCreateSensorSessionTypeTable(){
        StringBuilder builder = new StringBuilder("Create table ");
        builder.append(SensorDBSchema.SensorSessionTypeTable.NAME).append("( ")
                .append(SensorDBSchema.SensorSessionTypeTable.Cols.ID).append(" integer primary key autoincrement, ")
                .append(SensorDBSchema.SensorSessionTypeTable.Cols.NAME)
                .append(")");
        return builder.toString();
    }

    private String getCreateSensorTable(){
        StringBuilder builder = new StringBuilder("Create table ");
        builder.append(SensorDBSchema.SensorTable.NAME).append("( ")
                .append(SensorDBSchema.SensorTable.Cols.TYPE).append(" integer primary key, ")
                .append(SensorDBSchema.SensorTable.Cols.NAME)
                .append(")");
        return builder.toString();
    }

    private String getCreateSensorValueTable(){
        StringBuilder builder = new StringBuilder("Create table ");
        builder.append(SensorDBSchema.SensorValueTable.NAME).append("(")
                .append(" _id integer primary key, ")
                // Foreign key which connects to te sensor table
                .append(SensorDBSchema.SensorValueTable.Cols.TYPE).append(", ")
                .append(SensorDBSchema.SensorValueTable.Cols.TIMESTAMP).append(", ")
                .append(SensorDBSchema.SensorValueTable.Cols.VALUE).append(", ")
                .append(SensorDBSchema.SensorValueTable.Cols.VALUE_INDEX).append(", ")
                .append(SensorDBSchema.SensorValueTable.Cols.SESSION_ID).append(", ")
                .append(SensorDBSchema.SensorValueTable.Cols.SESSION_SEQUENCE_ID).append(", ")
                .append("FOREIGN KEY(").append(SensorDBSchema.SensorValueTable.Cols.TYPE)
                .append(") REFERENCES ").append(SensorDBSchema.SensorTable.NAME).append("(")
                .append(SensorDBSchema.SensorTable.Cols.TYPE).append(")")
                .append(")");

        return builder.toString();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
